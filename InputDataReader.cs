﻿using System;
using System.Diagnostics;
using System.IO;

namespace TetkinSergeyMetro
{
	class InputDataReader
	{
		public int MaxNodeCount;

		public InputData ReadInputData(string fnInput)
		{
			using (var sr = File.OpenText(fnInput))
			{
				var firstLine = sr.ReadLine();

				int n;
				int m;
				if (!ParseToTwoInts(firstLine, out n, out m)) throw new Exception("Первая строка входного файла должна содержать два целых числа, разделенных пробелом: параметры N и M.");

				if (n < 1 || n > MaxNodeCount) throw new Exception(string.Format("Параметр N (кол-во станций) должен быть в диапазоне от 1 до {0}.", MaxNodeCount));
				if (m < 0) throw new Exception("Параметр M (кол-во линий) должно быть положительным числом.");

				var lineInfos = new LineInfo[m];

				for (int i = 0; i < m; i++)
				{
					var line = sr.ReadLine();

					var lineNum = i + 1; // номер линии метро

					int nodeNum1;
					int nodeNum2;
					if (!ParseToTwoInts(line, out nodeNum1, out nodeNum2))
					{
						var errMsg = string.Format(
							"Не удалось прочитать данные для линии №{0}. Строка входного файла должна содержать два целых числа, разделенных пробелом.",
							lineNum);

						throw new Exception(errMsg);
					}

					ValidateNodeNum(n, lineNum, nodeNum1);
					ValidateNodeNum(n, lineNum, nodeNum2);

					lineInfos[i] = new LineInfo
					{
						NodeNum1 = nodeNum1,
						NodeNum2 = nodeNum2,
					};
				}

				return new InputData
				{
					N = n,
					LineInfos = lineInfos,
				};
			}
		}

		private void ValidateNodeNum(int n, int lineNum, int nodeNum)
		{
			if (nodeNum < 1 || nodeNum > n)
			{
				throw new Exception(string.Format(
					"Неверные данные для линии №{0}. Номер станции должен лежать в диапазоне от 1 до {1}",
					lineNum,
					n));
			}
		}
		private bool ParseToTwoInts(string line, out int n1, out int n2)
		{
			n1 = 0;
			n2 = 0;
			if (string.IsNullOrEmpty(line)) return false;

			var parts = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
			if (parts.Length < 2) return false;

			if (!int.TryParse(parts[0], out n1)) return false;
			if (!int.TryParse(parts[1], out n2)) return false;

			return true;
		}
	}

	class InputData
	{
		public int N;
		public LineInfo[] LineInfos;
	}

	[DebuggerDisplay("{NodeIndex1}-{NodeIndex2}")]
	class LineInfo
	{
		public int NodeNum1;
		public int NodeNum2;
	}
}
