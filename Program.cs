﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace TetkinSergeyMetro
{
	public class Program
	{
		private const int MaxNodeCount = 1000;

		public static void Main(string[] args)
		{
			try
			{
				var fnInput = args.Length >= 1 ? args[0] : "input.txt";
				var fnOutput = args.Length >= 2 ? args[1] : "output.txt";

				var r = new InputDataReader { MaxNodeCount = MaxNodeCount };
				var inputParams = r.ReadInputData(fnInput);

				var g = new MetroGraph();
				g.Setup(inputParams);

				var nums = g.EnumerateLeaves().Select(num => num.ToString());

				File.WriteAllLines(fnOutput, nums); // файл перезаписывается без проверки

#if DEBUG
				Diags(g);
#endif
			}
			catch (Exception x)
			{
				Log(x.Message);
			}
		}
		private static void Diags(MetroGraph g)
		{
			//var nums = g.EnumerateLeaves().ToArray();

			foreach (var num in g.EnumerateLeaves())
			{
				Debug.WriteLine(string.Format("{0}", num));
			}

			if (g.SomeNotVisited)
			{
				Log("Граф несвязный");
			}
		}
		private static void Log(string msg)
		{
			Debug.WriteLine(msg);
			Console.WriteLine(msg);
		}
	}
}
