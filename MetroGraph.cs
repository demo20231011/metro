﻿using System.Collections.Generic;
using System.Linq;

namespace TetkinSergeyMetro
{
	class MetroGraph
	{
		private NodeInfo[] _Nodes;
		private bool _IsVisitedDirty;

		public void Setup(InputData inputParams)
		{
			_Nodes = new NodeInfo[inputParams.N];
			_IsVisitedDirty = false;

			for (int nodeNum = 1; nodeNum <= _Nodes.Length; nodeNum++)
			{
				SetNodeByNum(nodeNum, new NodeInfo(nodeNum));
			}

			foreach (var li in inputParams.LineInfos)
			{
				var node1 = GetNodeByNum(li.NodeNum1);
				var node2 = GetNodeByNum(li.NodeNum2);

				node1.LinkTo(node2);
				node2.LinkTo(node1);
			}
		}
		private NodeInfo GetNodeByNum(int nodeNum)
		{
			var index = GetNodeIndexFromNum(nodeNum);
			return _Nodes[index];
		}
		private void SetNodeByNum(int nodeNum, NodeInfo node)
		{
			var index = GetNodeIndexFromNum(nodeNum);
			_Nodes[index] = node;
		}
		private int GetNodeIndexFromNum(int nodeNum)
		{
			return nodeNum - 1;
		}

		public IEnumerable<int> EnumerateLeaves()
		{
			// используем свой stack вместо рекурсии, чтобы избежать переполнения стэка вызовов для больших N 

			if (_IsVisitedDirty) ResetVisited(); // на случай, если метод будет вызван повторно

			var stack = new Stack<NodeInfo>();

			var nodeFirst = _Nodes.First();
			nodeFirst.Visited = true;
			stack.Push(nodeFirst);

			_IsVisitedDirty = true;

			while (stack.Count > 0)
			{
				var node = stack.Peek();

				var added = false;

				var c = node.LinkedNodes.Where(n => !n.Visited).ToArray();
				foreach (var n in c)
				{
					n.Visited = true;
					stack.Push(n);
					added = true;
				}

				if (!added)
				{
					stack.Pop();

					yield return node.NodeNum;
				}
			}
		}
		public bool SomeNotVisited
		{
			get { return _Nodes.Where(n => !n.Visited).Any(); }
		}
		private void ResetVisited()
		{
			foreach (var node in _Nodes)
			{
				node.Visited = false;
			}
			_IsVisitedDirty = false;
		}

		class NodeInfo
		{
			// используем Dictionary, на случай, если в исходных данных будут повторяющиеся связки между узлами
			private Dictionary<int, NodeInfo> _LinkedNodes = new Dictionary<int, NodeInfo>();

			public bool Visited;

			public int NodeNum { get; private set; }

			public IEnumerable<NodeInfo> LinkedNodes
			{
				get { return _LinkedNodes.Values; }
			}

			public NodeInfo(int nodeNum)
			{
				NodeNum = nodeNum;
			}
			public void LinkTo(NodeInfo node)
			{
				if (!_LinkedNodes.ContainsKey(node.NodeNum)) _LinkedNodes.Add(node.NodeNum, node);
			}

			public override string ToString()
			{
				return string.Format("{0}{1} -> {2}",
					Visited ? "+" : " ",
					NodeNum,
					string.Join(", ", _LinkedNodes.Values.Select(ni => ni.NodeNum)));
			}
		}
	}
}
